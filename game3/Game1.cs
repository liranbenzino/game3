﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;
using VelcroPhysics.Utilities;

namespace game3
{

    public class Game1 : Game
    {
        private readonly GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private KeyboardState oldKeyState;
        private SpriteFont font;

        // This keyword prevents a field from being changed. Readonly fields can be initialized at runtime, unlike const values.
        // Attempts to change them later are disallowed
        private readonly World world;

        private Body circleBody;
        private Body groundBody;
        private Body groundBody2;
        private Body groundBody3;

        private Texture2D circleSprite;
        private Texture2D groundSprite;

        // Simple camera controls
        private Matrix view;

        private Vector2 cameraPosition;
        private Vector2 screenCenter;
        private Vector2 groundOrigin;
        private Vector2 circleOrigin;


        private const string Text = "Use arrow keys to move and space to jump";

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 1024;
            graphics.PreferredBackBufferWidth = 1000;

            Content.RootDirectory = "Content";

            //Create a world with gravity.
            world = new World(new Vector2(0, 9.82f));
        }

        protected override void Initialize()
        {

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Initialize camera controls
            view = Matrix.Identity;
            cameraPosition = Vector2.Zero;
            screenCenter = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2f, graphics.GraphicsDevice.Viewport.Height / 2f);
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);

            font = Content.Load<SpriteFont>("font");

            // Load sprites
            //circleSprite = Content.Load<Texture2D>("CircleSprite"); //  96px x 96px => 1.5m x 1.5m
            circleSprite = Content.Load<Texture2D>("SoccerBall"); //  96px x 96px => 1.5m x 1.5m
            //groundSprite = Content.Load<Texture2D>("GroundSprite"); // 512px x 64px =>   8m x 1m
            //groundSprite = Content.Load<Texture2D>("steelstrip"); // 512px x 64px =>   8m x 1m
            groundSprite = Content.Load<Texture2D>("block_tiles_red"); // 512px x 64px =>   8m x 1m



            /* We need XNA to draw the ground and circle at the center of the shapes */
            groundOrigin = new Vector2(groundSprite.Width / 2f, groundSprite.Height / 2f);
            circleOrigin = new Vector2(circleSprite.Width / 2f, circleSprite.Height / 2f);

            // Velcro Physics expects objects to be scaled to MKS (meters, kilos, seconds)
            // 1 meters equals 64 pixels here
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            /* Circle */
            // Convert screen center from pixels to meters
            Vector2 circlePosition = ConvertUnits.ToSimUnits(screenCenter) + new Vector2(0, -1.5f);

            // Create the circle fixture
            circleBody = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(96 / 2f), 1f, circlePosition, BodyType.Dynamic);

            // Give it some bounce and friction
           // circleBody.Restitution = 0.3f;
            //circleBody.Friction = 0.5f;

            /* Ground */
            Vector2 groundPosition = ConvertUnits.ToSimUnits(screenCenter) + new Vector2(-2, 1.25f);

            // Create the ground fixture
            groundBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, groundPosition);
            groundBody.BodyType = BodyType.Static;
            groundBody.Restitution = 0.3f;
            groundBody.Friction = 0.5f; 
            
            /* Ground */
            Vector2 groundPosition2 = ConvertUnits.ToSimUnits(screenCenter) + new Vector2(5, 2f);

            // Create the ground fixture
            groundBody2 = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, groundPosition2);
            groundBody2.BodyType = BodyType.Static;
            groundBody2.Restitution = 0.3f;
            groundBody2.Friction = 0.5f;
            
            /* Ground */
            Vector2 groundPosition3 = ConvertUnits.ToSimUnits(screenCenter) + new Vector2(12, 5f);

            // Create the ground fixture
            groundBody3 = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, groundPosition3);
            groundBody3.BodyType = BodyType.Static;
            groundBody3.Restitution = 0.3f;
            groundBody3.Friction = 0.5f;
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            HandleKeyboard();

            //We update the world
            world.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);

            base.Update(gameTime);
        }

        private void HandleKeyboard()
        {
            KeyboardState state = Keyboard.GetState();

            // Move camera
            if (state.IsKeyDown(Keys.Left))
                cameraPosition.X += 5f;

            if (state.IsKeyDown(Keys.Right))
                cameraPosition.X -= 5f;

            if (state.IsKeyDown(Keys.Up))
                cameraPosition.Y += 1.5f;

            if (state.IsKeyDown(Keys.Down))
                cameraPosition.Y -= 1.5f;

             view = Matrix.CreateTranslation(new Vector3(cameraPosition - screenCenter, 0f)) * Matrix.CreateTranslation(new Vector3(screenCenter, 0f));

            // We make it possible to rotate the circle body
            if (state.IsKeyDown(Keys.Left))
                circleBody.ApplyTorque(-3);

            if (state.IsKeyDown(Keys.Right))
                circleBody.ApplyTorque(3);

            if (state.IsKeyDown(Keys.Space) && oldKeyState.IsKeyUp(Keys.Space))
                circleBody.ApplyLinearImpulse(new Vector2(0, -10));

            if (state.IsKeyDown(Keys.Escape))
                Exit();

            oldKeyState = state;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Transparent);

            //Draw circle and ground
            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, view);
            spriteBatch.Draw(circleSprite, ConvertUnits.ToDisplayUnits(circleBody.Position), null, Color.White, circleBody.Rotation, circleOrigin, 1f, SpriteEffects.None, 0f);
            spriteBatch.Draw(groundSprite, ConvertUnits.ToDisplayUnits(groundBody.Position), null, Color.White, 0f, groundOrigin, 1f, SpriteEffects.None, 0f);
            spriteBatch.Draw(groundSprite, ConvertUnits.ToDisplayUnits(groundBody2.Position), null, Color.White, 0f, groundOrigin, 1f, SpriteEffects.None, 0f);
            spriteBatch.Draw(groundSprite, ConvertUnits.ToDisplayUnits(groundBody3.Position), null, Color.White, 0f, groundOrigin, 1f, SpriteEffects.None, 0f);
            spriteBatch.End();

            // Display instructions
            spriteBatch.Begin();
            spriteBatch.DrawString(font, Text, new Vector2(12f, 12f), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
